export default {
  search: items => items.map(app => ({
    caseNumber: app.app_number,
    caseTitle: app.app_title,
    process: app.app_pro_title,
    task: app.app_tas_title,
    sentBy: app.app_del_previous_user,
    dueDate: app.del_task_due_date,
    status: app.app_status,
    pro_uid: app.pro_uid,
    task_uid: app.tas_uid,
    app_uid: app.app_uid,
    del_index: app.del_index
  })),

  inbox: items => items
    .filter(app => app.app_status !== 'DRAFT')
    .map(app => ({
      caseNumber: app.app_number,
      caseTitle: app.app_title,
      process: app.app_pro_title,
      task: app.app_tas_title,
      sentBy: app.app_del_previous_user,
      dueDate: app.del_task_due_date,
      status: app.app_status_label,
      pro_uid: app.pro_uid,
      task_uid: app.tas_uid,
      app_uid: app.app_uid,
      del_index: app.del_index,
      creator: app.usrcr_usr_lastname + ', ' + app.usrcr_usr_firstname,
      created: app.app_create_date,
      updated: app.app_update_date,
      description: 'N/A',
      currentTask: {
        title: app.app_tas_title,
        delegated: app.del_delegate_date,
        initiated: app.del_init_date,
        duedate: app.del_task_due_date,
        finished: app.del_finish_date || '-',
        user: app.app_current_user
      }
    })),

  processes: (items) => items
    .filter(process => process.prj_status !== 'INACTIVE')
    .map(process => ({
      id: process.pro_uid,
      title: process.pro_title
    })),

  user: user => ({
    username: user.username,
    email: user.mail,
    photo: user.photo,
    uid: user.uid,
    firstname: user.firstname,
    lastname: user.lastname
  })
}
