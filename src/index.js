import axios from 'axios'
import memoize from 'p-memoize'
import transformer from './transformer'

export default class ProcessMaker {
  constructor (config) {
    this.base = config.serverUri + 'api/1.0/' + config.workspace
    this.config = config
    this.token = window.localStorage.getItem('token')

    if (this.token !== null) {
      axios.defaults.headers.common.Authorization = 'Bearer ' + this.token
    }

    this.get = memoize(axios.get)
  }

  async getTasks (app) {
    if (!app) {
      return
    }

    try {
      const { data } = await this.get(`${this.base}/cases/${app}/tasks`)

      return { tasks: data }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getProcesses () {
    try {
      const { data } = await this.get(`${this.base}/case/start-cases`)

      return { processes: transformer.processes(data) }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async createCase (process) {
    if (!process) {
      return
    }

    try {
      const response = await this.get(`${this.base}/case/start-cases`)
      const startingTask = response.data.filter(task => task.pro_uid === process)[0].tas_uid

      const { data } = await axios.post(`${this.base}/cases`, {
        pro_uid: process,
        tas_uid: startingTask
      })

      return { ...data, startingTask }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getTodo () {
    try {
      const { data } = await this.get(`${this.base}/cases`)
      return { inbox: transformer.inbox(data) }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getHistory () {
    try {
      const { data } = await this.get(`${this.base}/cases/participated`)

      return { inbox: transformer.inbox(data) }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getUnassigned () {
    try {
      const { data } = await this.get(`${this.base}/cases/unassigned`)

      return { inbox: transformer.inbox(data) }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async assignCase (app) {
    if (!app) {
      throw new Error('Missing required parameters.')
    }

    try {
      return axios.post(`${this.base}/extrarest/case/${app}/claim`)
    } catch (exception) {
      throw new Error(exception.response.data.error.message)
    }
  }

  async getSummary (item) {
    if (!item) {
      return
    }

    try {
      return {
        summary: {
          case: item,
          task: item.currentTask
        }
      }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async uploadFile (app, task, inputDoc, file) {
    if (!app) {
      return
    }

    try {
      const formData = new window.FormData()

      formData.set('form', file.file)
      formData.set('tas_uid', task)
      formData.set('app_doc_comment', 'N/A')
      formData.set('inp_doc_uid', inputDoc)

      return await axios.post(`${this.base}/cases/${app}/input-document`, formData, {
        headers: { 'Content-Type': 'multipart/form-data' }
      })
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getInputFiles (app) {
    if (!app) {
      return
    }

    try {
      const { data } = await this.get(`${this.base}/cases/${app}/input-documents`)

      return { files: data }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getOutputFiles (app) {
    if (!app) {
      return
    }

    try {
      const { data } = await this.get(`${this.base}/cases/${app}/output-documents`)

      return { files: data }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getSteps (app, task) {
    if (!app || !task) {
      return
    }

    try {
      const { data } = await this.get(`${this.base}/project/${app}/activity/${task}/steps`)

      return { steps: data }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getCaseNotes (app) {
    if (!app) {
      return
    }

    try {
      const { data } = await this.get(`${this.base}/cases/${app}/notes`)

      return { caseNotes: data }
    } catch (exception) {
      if (exception.response.status === 400) {
        return { caseNotes: [] }
      }

      throw new Error(exception)
    }
  }

  async saveCaseNote (app, note) {
    if (!app || !note) {
      return
    }

    try {
      return axios.post(`${this.base}/cases/${app}/note`, {
        note_content: note
      })
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async search (keyword) {
    try {
      const { data } = await this.get(`${this.base}/cases/advanced-search?search=${keyword}`)

      return { searchResults: transformer.search(data) }
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async saveVariables (app, variables) {
    if (!app) {
      return
    }

    try {
      return await axios.put(`${this.base}/cases/${app}/variable`, variables)
    } catch (exception) {
      throw new Error(exception.response.data.error_description)
    }
  }

  async executeQuery (process, field, variable, dynaformUid) {
    if (!process || !variable) {
      return []
    }

    try {
      const { data } = await axios.post(`${this.base}/project/${process}/process-variable/${variable}/execute-query`, {
        dyn_uid: dynaformUid,
        field_id: field
      })

      return data
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async executeQuerySuggest (process, field, variable, filter, dynaformUid) {
    if (!process || !variable) {
      return []
    }

    try {
      const { data } = await axios.post(`${this.base}/project/${process}/process-variable/${variable}/execute-query-suggest`, {
        dyn_uid: dynaformUid,
        field_id: field,
        filter
      })

      return data
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async getDynaform (process, task, app, del, step) {
    if (!app) {
      return
    }

    try {
      const { data: variables } = await this.get(`${this.base}/cases/${app}/variables?app_index=${del}`)
      const { data: steps } = await this.get(`${this.base}/project/${process}/activity/${task}/steps`)
      const dynaform = await this.get(`${this.base}/project/${process}/dynaform/${steps[step].step_uid_obj}`)

      return {
        variables,
        dynaform: { uid: dynaform.data.dyn_uid, dynaform: JSON.parse(dynaform.data.dyn_content) }
      }
    } catch (error) {
      if (error.response.status === 403) {
        const { data: { pro_summary_dynaform: uid } } = await this.get(`${this.base}/project/${process}/process`)
        const { data: { dyn_content: dynaform } } = await this.get(`${this.base}/project/${process}/dynaform/${uid}`)

        return {
          variables: null,
          dynaform: { uid: dynaform, dynaform: JSON.parse(dynaform) }
        }
      }
    }
  }

  async submitDynaform (app) {
    if (!app) {
      return
    }

    try {
      const { data } = await axios.put(`${this.base}/cases/${app}/route-case`)

      return data
    } catch (exception) {
      throw new Error(exception.response.data.error.message)
    }
  }

  async getUserInfo (uid) {
    if (!uid) {
      return
    }

    try {
      const { data: user } = await this.get(`${this.base}/extrarest/user?usr_uid=${uid}`)

      return { user, uid: uid }
    } catch (exception) {
      throw new Error(exception.response.data.error.message)
    }
  }

  async getCurrentUser (username) {
    if (!username) {
      return
    }

    try {
      const { data: user } = await this.get(`${this.base}/extrarest/login-user`)

      return transformer.user(user)
    } catch (exception) {
      throw new Error(exception)
    }
  }

  async login (credentials) {
    if (!credentials) {
      return
    }

    try {
      const { data: { access_token: token } } = await axios.post(this.base + '/oauth2/token', {
        ...credentials,
        ...this.config.credentials
      })

      this.token = token

      window.localStorage.setItem('token', this.token)
      axios.defaults.headers.common.Authorization = 'Bearer ' + this.token
    } catch (exception) {
      throw new Error(exception.response.data.error_description)
    }
  }
}
